version_info = (3, 0, 0, 'b2')
version = '3.0.0'
release = '3.0.0b2'

__version__ = release  # PEP 396
